import DashboardLayout from '../layouts/DashboardLayout'
import Profile from '../pages/profile/UserProfile'

export const profileMenu = {
  path: '/profile',
  component: DashboardLayout,
  name: 'Profile',
  redirect: '/profile',
  children: [
    {
      path: '/profile',
      name: 'MyProfile',
      components: { default: Profile },
      meta: {
        requiresAuth: true,
        roles: 'all',
        breadcrumb: [{
          title: 'Thông tin cá nhân'
        }]
      },
    },
    {
      path: '/mobile/profile',
      name: 'MyProfile',
      components: { default: Profile },
      meta: {
        requiresAuth: true,
        roles: 'all',
        breadcrumb: [{
          title: 'Thông tin cá nhân'
        }]
      },
    }
  ],
}