import Vue from 'vue'
import VueRouter from 'vue-router'
import TokenService from '../helpers/token'
import { updateProfileInto, checkMobile } from '../helpers/utils'
import RepositoryFactory from '../repositories/RepositoryFactory'
import AuthLayout from '../layouts/AuthLayout'

// pages
import Error403 from '../pages/Error403'
import Login from '../pages/Login'

const AuthRepository = RepositoryFactory.get('auth')

const { userMenu } = require('./user')
const { dataSetting } = require('./dataSetting')
const { profileMenu } = require('./profile')

const permission = TokenService.getPermissions();

const authPages = {
  path: '/',
  component: AuthLayout,
  name: 'Authentication',
  children: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {
        onlyWhenLoggedOut: true,
        public: true,
      },
    },
    {
      path: '/error-403',
      name: 'Error403',
      component: Error403,
      meta: {
        layout: 'login',
        public: true,
      },
    },
  ],
}

const routes = [
  {
    path: '/',
    redirect: '/users/user-list',
    name: 'Home',
  },
  authPages,
  userMenu,
  dataSetting,
  profileMenu
]
Vue.use(VueRouter)

// configure router
const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior: (to) => {
    if (to.hash) {
      return { selector: to.hash }
    }
    return { x: 0, y: 0 }
  },
  linkExactActiveClass: 'nav-item active',
})

// eslint-disable-next-line consistent-return
router.beforeEach(async (to, from, next) => {
  const isPublic = _.get(to.meta, 'public', false)
  const onlyWhenLoggedOut = _.get(to.meta, 'onlyWhenLoggedOut', false)
  const loggedIn = !!TokenService.getToken()

  if (isPublic) next()
  if (!isPublic && !loggedIn) {
    return next({
      path: '/login',
      query: { redirect: to.fullPath }, // Store the full path to redirect the user to after login
    })
  }

  // Do not allow user to visit entry page if they are logged in
  if (loggedIn && onlyWhenLoggedOut) {
    return next('/users/user-list')
  }

  // check role of user
  if (loggedIn) {
    // get user's profile
    const profile = await AuthRepository.profile()
    updateProfileInto(_.get(profile, 'data.profile'))
    // check permission
    const metaRole = _.get(to.meta, 'roles', []);
    const userPermissions = TokenService.getPermissions();
    next()
    // Do not allow user to visit entry page if they are logged in or has not according role
    if (metaRole === 'all') {
      next();
    } else if (!_.intersection(metaRole, userPermissions).length) {
      return next('/error-403');
    }
  }
  next()
})

export default router
