import DashboardLayout from '../layouts/DashboardLayout'
import RoleList from '../pages/roles/RoleList'
import RoleForm from '../pages/roles/RoleForm'

export const dataSetting = {
  path: '/data-setting',
  component: DashboardLayout,
  name: 'DataSetting',
  redirect: '/data-setting',
  children: [
    {
      path: 'role',
      name: 'RoleList',
      components: { default: RoleList },
      meta: {
        roles: ['role_read','role_create','role_update','role_delete'],
        requiresAuth: true,
        breadcrumb: [{
          title: 'Danh sách quyền',
        }]
      },
    },
    {
      path: 'create-role',
      name: 'CreateRole',
      components: { default: RoleForm },
      meta: {
        roles: ['role_create'],
        requiresAuth: true,
        breadcrumb: [{
          title: 'Danh sách quyền',
          to: '/data-setting/role'
        }, {
          title: 'Tạo mới quyền',
        }]
      },
    },
    {
      path: 'role/edit/:id',
      name: 'EditRole',
      components: { default: RoleForm },
      meta: {
        roles: ['role_update'],
        requiresAuth: true,
        breadcrumb: [{
          title: 'Danh sách quyền',
          to: '/data-setting/role'
        }, {
          title: 'Chỉnh sửa quyền',
        }]
      },
    }
  ],
}