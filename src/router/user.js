import DashboardLayout from '../layouts/DashboardLayout'
import UserList from '../pages/users/UserList'

export const userMenu = {
  path: '/users',
  component: DashboardLayout,
  name: 'Users',
  redirect: '/users',
  children: [
    {
      path: 'user-list',
      name: 'UserList',
      components: { default: UserList },
      meta: {
        roles: ['user_read','user_create','user_update','user_delete', 'user_view_all'],
        requiresAuth: true,
        breadcrumb: [{
          title: 'Danh sách người dùng',
        }]
      },
    }
  ],
}
