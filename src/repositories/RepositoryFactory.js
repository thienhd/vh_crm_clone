import AddressRepository from './AddressRepository';
import AuthRepository from './AuthRepository';
import UserRepository from './UserRepository';
import RoleRepository from './RoleRepository';
import ProductGroupRepository from './ProductGroupRepository';
import ImageRepository from './ImageRepository';
import DepartmentRepository from './DepartmentRepository';

const repositories = {
  address: AddressRepository,
  auth: AuthRepository,
  user: UserRepository,
  role: RoleRepository,
  product_group: ProductGroupRepository,
  image: ImageRepository,
  department: DepartmentRepository
};

export default {
  get: (name) => repositories[name],
};
