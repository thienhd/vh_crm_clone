export const models = [
    {
        name: 'contact_managment',
        text: 'Khách hàng',
        permissions: [],
        level: 1,
    },
    {
        name: 'contact',
        text: 'Thông tin khách hàng',
        permissions: ['read', 'create', 'update', 'delete', 'view_all', 'download'],
        level: 2,
    },
    {
        name: 'contact_change_user',
        text: 'Chuyển người phụ trách',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'contact_change_status',
        text: 'Chuyển mối quan hệ',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'contact_change_phone',
        text: 'Chuyển số điện thoại',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'contact_move_to_cc',
        text: 'Thu hồi contact về kho CC',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'search_all',
        text: 'Tìm kiếm tất cả',
        permissions: ['read'],
        level: 2
    },
    {
        name: 'merge-contact',
        text: 'Gộp contact',
        permissions: ['read'],
        level: 2
    },
    {
        name: 'admin-search',
        text: 'Tìm kiếm cấp admin',
        permissions: ['read'],
        level: 2
    },
    {
        name: 'user_managment',
        text: 'Quản lý người dùng',
        permissions: [],
        level: 1,
    },
    {
        name: 'user',
        text: 'Người dùng',
        permissions: ['read', 'create', 'update', 'delete', 'view_all'],
        level: 2,
    },
    {
        name: 'department',
        text: 'Phòng ban',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'kpi',
        text: 'KPI',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'schedule',
        text: 'Lịch làm việc',
        permissions: ['read', 'create', 'update', 'delete', 'view_all'],
        level: 2,
    },
    {
        name: 'appointment',
        text: 'Lịch hẹn',
        permissions: ['read', 'create', 'update', 'delete', 'view_all'],
        level: 2,
    },
    {
        name: 'product_management',
        text: 'Quản lý sản phẩm',
        permissions: [],
        level: 1,
    },
    {
        name: 'product',
        text: 'Sản phẩm/Nhóm sản phẩm/Combo',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'call_center_management',
        text: 'Call Center',
        permissions: [],
        level: 1,
    },
    {
        name: 'call_center',
        text: 'Danh sách cuộc gọi',
        permissions: ['read', 'download', 'view_all'],
        level: 2,
    },
    {
        name: 'general-call-center-report',
        text: 'Báo cáo chung',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'employee-call-center-report',
        text: 'Báo cáo nhân viên',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'download-call-center-record',
        text: 'Download file ghi âm',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'order_management',
        text: 'Đơn hàng',
        permissions: [],
        level: 1,
    },
    {
        name: 'order',
        text: 'Đơn hàng',
        permissions: ['read', 'create', 'update', 'delete', 'view_all'],
        level: 2,
    },
    {
        name: 'order-ctv-create',
        text: 'CTV tạo đơn',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'order-contact',
        text: 'Xem tài khoản',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'order-payment',
        text: 'Xác nhận thanh toán',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'report',
        text: 'Báo cáo',
        permissions: [],
        level: 1,
    },

    {
        name: 'department-report',
        text: 'Báo cáo phòng ban',
        permissions: [],
        level: 2,
    },
    {
        name: 'result-report',
        text: 'Theo kết quả',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'employee-report',
        text: 'Theo nhân viên',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'status-report',
        text: 'Theo trạng thái',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'contact-source-report',
        text: 'Theo nguồn contact',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'best-employee',
        text: 'Báo cáo nhân viên xuất sắc',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'sale-leader',
        text: 'Báo cáo trưởng nhóm',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'revenue-report',
        text: 'Báo cáo doanh thu',
        permissions: [],
        level: 2,
    },
    {
        name: 'product-group-revenue-report',
        text: 'Theo sản phẩm, nhóm sản phẩm',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'customer-revenue-report',
        text: 'Theo khách hàng',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'personal-report',
        text: 'Báo cáo thành tích cá nhân',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'contact-register',
        text: 'Báo cáo đăng ký theo ngày',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'delivery-report',
        text: 'Báo cáo giao hàng',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'a-department-report',
        text: 'Báo cáo phòng ban trực thuộc',
        permissions: [],
        level: 2,
    },
    {
        name: 'employee-report-a-department',
        text: 'Theo nhân viên',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'view-total-report',
        text: 'View tổng',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'data_setting_management',
        text: 'Cài đặt dữ liệu',
        permissions: [],
        level: 1,
    },
    {
        name: 'contact-setting',
        text: 'Khách hàng',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'contact-source',
        text: 'Nguồn khách hàng',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'contact-status',
        text: 'Mối quan hệ',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'role',
        text: 'Nhóm quyền',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'announcement',
        text: 'Thông báo',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'school-level',
        text: 'Nhóm ngành',
        permissions: ['read', 'create', 'update', 'delete'],
        level: 2,
    },
    {
        name: 'setting',
        text: 'Cấu hình',
        permissions: [],
        level: 1,
    },
    {
        name: 'setting-contact',
        text: 'Phân bổ contact',
        permissions: ['read', 'update'],
        level: 2,
    },
    {
        name: 'dashboard_management',
        text: 'Dashboard',
        permissions: [],
        level: 1,
    },
    {
        name: 'dashboard',
        text: 'Dashboard',
        permissions: ['read'],
        level: 2,
    },
    {
        name: 'mkt-sms',
        text: 'Marketing SMS',
        permissions: [],
        level: 2,
    },
    {
        name: 'mkt-sms-message',
        text: 'Tin nhắn',
        permissions: ['read', 'update', 'create', 'delete'],
        level: 3,
    },
    {
        name: 'mkt-sms-customer',
        text: 'Tệp khách hàng',
        permissions: ['read', 'update', 'create', 'delete'],
        level: 3,
    },
    {
        name: 'mkt-sms-contact',
        text: 'Xem danh sách contact của tệp',
        permissions: ['read'],
        level: 3,
    },
    {
        name: 'mkt-sms-message-history',
        text: 'Lích sử gửi tin',
        permissions: ['read'],
        level: 3,
    }
];

export const actions = [{
    name: 'read',
    text: 'Truy cập',
}, {
    name: 'create',
    text: 'Thêm'
}, {
    name: 'update',
    text: 'Sửa'
}, {
    name: 'delete',
    text: 'Xóa'
}, {
    name: 'download',
    text: 'Download/Upload'
}, {
    name: 'view_all',
    text: 'Xem tất cả'
}];


export function getPermissionByAction(action) {
    return models.filter(m => m.permissions.includes(action)).map(model => `${action}-${model.name}`)
}