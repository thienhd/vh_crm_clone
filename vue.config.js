const buildID = `_${Date.now()}`;
module.exports = {
  chainWebpack: config => {
    config.output.chunkFilename(`js/[name]${buildID}.js`)
  },
  css: {
    extract: {
      filename: `[name]${buildID}.css`,
    },
  },
}