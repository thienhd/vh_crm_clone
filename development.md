# Giới thiệu
- Project cho phần frontend của hệ thống VH-CRM
- Framework Vue 2.6
- Theme vue-material-dashboard-pro

# Các thư viện sử dụng trong dự án
* axios: https://github.com/axios/axios
* socket.io-client: https://socket.io/docs/client-api/
* vuetify: https://vuetifyjs.com/vi-VN/
* vue: https://vi.vuejs.org/
* chartist https://gionkunz.github.io/chartist-js/
* VeeValidate https://logaretm.github.io/vee-validate/

# Cấu trúc thư mục

* `src` folder contains all logic code
    - `components`: Chứa các components sử dụng chung cho hệ thống
        + `base`: các components của theme
        + `common`: Các components custom thường hay sử dụng trong hệ thống
    - `helpers`: Chứa các function/variable utils thường hay sử dụng trong hệ thống
    - `layout`: Chứa các header, footer, menu ở hệ thống
    - `mixins`: chứa các function hay được sử dụng trong hệ thống
    - `page`: Chứa components các phần của hệ thống
        + `appointments`: Components cho phần lịch hẹn
        + `callCenter`: Components cho phần call center
        + `contact`: Components cho phần khách hàng
        + `contact-source`: Components cho phần nguồn khách hàng
        + `contact-status`: Components cho phần mối quan hệ
        + `departments`: Components cho phần phòng ban
        + `kpi`: Components cho phần KPI
        + `order`: Components cho phần đơn hàng
        + `products`: Components cho phần sản phẩm
        + `profile`: Components cho phần profile
        + `report`: Components cho phần báo cáo
        + `roles`: Components cho phần phân quyền
        + `schedules`: Components cho phần lịch làm việc
        + `setting`: Components cho phần cấu hình
        + `users`: Components cho phần người dùng (nhân viên)
    - `CallCenter.vue`: Component của Call Center
    - `Error403.vue`: Component của trang Error403
    - `Login.vue`: Component của trang Login
    - `plugins`: Chứa các file thư viện sử dụng trong hệ thống
    - `repositories`: Nơi chứa các function để kết nối với API
    - `routes`: Nơi tạo các page cho website
    - `sass`: Nơi chứa file sass và scss để chỉnh giao diện webiste
    - `main.js`: File main project
* `.env.development.local` Các config url API 
