// $(window).bind('beforeunload', function (e) {
//     if (1 == 1) {
//         return 'abc';
//     }
// });

$(window).bind('unload', function () {
    csUnregister();
    if (csVoice.enableVoice) {
        reConfigDeviceType();
    }
});
var isExpiredCSToken = false
window.callStatusCareSoft = ''
window.incomingCallStatusCareSoft = ''
var isIncomingCall = true

// kết thúc cuộc gọi ra/vào
function csEndCall() {
    console.log("Call is ended");
    let sound = document.getElementById('incomingCallAudio')
    if (sound) {
        sound.pause()
        sound.remove()
    }
    localStorage.removeItem('csInCall')
    // document.getElementById('phoneNo').innerHTML = "";
    window.callStatusCareSoft = 'End call';
    window.incomingCallStatusCareSoft = 'End call';
    if (!isIncomingCall) {
        isIncomingCall = true
    }
    $('#transferCall').attr('disabled', 'disabled');
    $('#transferCallAcd').attr('disabled', 'disabled');
}

// đổ chuông trình duyệt của agent khi gọi ra
function csCallRingingAgent() {
    console.log("Has a new call to agent");
    window.callStatusCareSoft = 'Call ringing';
    window.incomingCallStatusCareSoft = 'Call ringing';
}

// đổ chuông trình duyệt của agent khi gọi vào
// đổ chuông tới khách hàng khi gọi ra
function csCallRinging(phone) {
    console.log("Has a new call to customer: " + phone);
    if (isIncomingCall) {
        let sound = document.getElementById('incomingCallAudio')
        if (!sound) {
            sound          = document.createElement('audio');
            sound.id       = 'incomingCallAudio';
            sound.controls = 'controls';
            sound.loop     = 'loop' 
            sound.autoplay = 'autoplay'
            sound.src      = 'https://freesound.org/data/previews/62/62872_634166-lq.mp3';
            sound.type     = 'audio/mpeg';
            sound.style    = 'display: none'
            sound.volume   = 0.5
            document.getElementById('app-bar').appendChild(sound)
        }
        Bus.$emit("incoming-call-cs", phone)
    }
    localStorage.setItem('csInCall', 1)
    // document.getElementById('phoneNo').innerHTML = phone + ' đang gọi ...';
}

// cuộc gọi vào được agent trả lời
function csAcceptCall() {
    console.log("Call is Accepted");
    // document.getElementById('phoneNo').innerHTML = "Đang trả lời";
    if (isIncomingCall) {
        window.callStatusCareSoft = 'Accept call';
        window.incomingCallStatusCareSoft = 'Accept call';
    }
    // window.callStatusCareSoft = 'Accept call';
    // $('#transferCall').removeAttr('disabled');
    // $('#transferCallAcd').removeAttr('disabled');

}

// cuộc gọi ra được khách hàng trả lời
function csCustomerAccept() {
    console.log("csCustomerAccept");
    window.callStatusCareSoft = 'Accept call';
    window.incomingCallStatusCareSoft = 'Accept call';
    // document.getElementById('phoneNo').innerHTML = "Đang trả lời";
}

function csMuteCall() {
    console.log("Call is muted");
}

function csUnMuteCall() {
    console.log("Call is unmuted")
}

function csHoldCall() {
    console.log("Call is holded");
}

function csUnHoldCall() {
    console.log("Call is unholded");
}

function showCalloutInfo(number) {
    console.log("callout to " + number)
    isIncomingCall = false
}

function showCalloutError(errorCode, sipCode) {
    console.log("callout error " + errorCode + " - " + sipCode);
    let sound = document.getElementById('incomingCallAudio')
    if (sound) {
        sound.pause()
        sound.remove()
    }
    localStorage.removeItem('csInCall')
    if (errorCode == 'IPCC_NOT_CONNECT_CUSTOMER') {
        window.callStatusCareSoft = 'failed'
        window.incomingCallStatusCareSoft = 'failed'
    } else {
        window.callStatusCareSoft = 'error'
        window.incomingCallStatusCareSoft = 'error'
    }
}

function csShowEnableVoice(enableVoice) {
    if (enableVoice) {
        console.log("voice enabled")
    } else {
        console.log("voice disabled")
        Bus.$emit('cs-voice-disabled')
    }
}

function csShowDeviceType(type) {
    console.log("csShowDeviceType");
}

function csShowCallStatus(status) {
    console.log("csShowCallStatus");
}

function csInitComplete() {
    if (!csVoice.enableVoice) {
        csEnableCall();
        console.log("enable call")
    }
    isExpiredCSToken = false
    console.log("csInitComplete");
}

function csCurrentCallId(callId) {
    console.log("csCurrentCallId: " + callId);
}

function csInitError(error) {
    console.log("csInitError: " + error);
    window.callStatusCareSoft = 'error';
    window.incomingCallStatusCareSoft = 'error';
    isExpiredCSToken = true
}

function csListTransferAgent(listTransferAgent) {
    console.log(listTransferAgent);
}
function csTransferCallError(error, tranferedAgentInfo) {
    console.log('Transfer call failed,' + error);
}
function csTransferCallSuccess(tranferedAgentInfo) {
    console.log('transfer call success');
}
function csNewCallTransferRequest(transferCall) {
    console.log('new call transfer');
    console.log(transferCall);
    document.getElementById('phoneNo').innerHTML = transferCall.dropAgentId + ' chuyển cg cho bạn';
    $('#transferResponseOK').removeAttr('disabled');
    $('#transferResponseReject').removeAttr('disabled');
}

function csTransferCallResponse(status) {
    $('#transferResponseOK').attr('disabled', 'disabled');
    $('#transferResponseReject').attr('disabled', 'disabled');
    console.log(status);
}

function csSetCookie(value, ext) {
    document.cookie = "csToken" + ext + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    document.cookie = "csToken" + ext + "=" + value + ";";
}

function csGetCookie(ext) {
    var name = "csToken" + ext + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function csGetCookieInCall() {
    var name = "csInCall=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}