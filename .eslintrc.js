module.exports = {
  root: true,
  globals: {
    _: true,
    Bus: true,
    Vue: true,
    socket: true,
  },
  env: {
    browser: true,
  },
  extends: ['plugin:vue/essential'],
  rules: {
    'max-len': ['warn', {'code': 300}],
    // "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    // "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-console": "off",
    "vue/valid-v-for": "off",
    "vue/require-v-for-key": "off",
  },
  plugins: [
    'vue'
  ],
  parserOptions: {
    parser: "babel-eslint"
  }
};
